# *********************************************************************************************
# Description: Script Python to launch music if sensor  motion are in good status
# Author: J.Vergereau
# Date: 26/03/2022
# *********************************************************************************************

import pygame as pg
import time
import RPi.GPIO as GPIO
import datetime

# Read on GPIO 7 Motion Sensor result
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.IN)


def launchMp3():
    
    # 1. Load mp3 sound  
    freq = 44100 # audio CD quality
    bitsize = -16 # unsigned 16 bit
    channels = 2 # 1 is mono, 2 is stereo
    buffer = 2048 # number of samples (experiment to get best sound)
    clock = pg.time.Clock()
    pg.mixer.init(freq, bitsize, channels, buffer)
    pg.mixer.music.set_volume(0.8)
    pg.mixer.music.load('/home/pi/xpertiz/comp3.mp3')
    
     # 2. play a short mp3 sound to notify Start of taking a Photo
    print ("Start play music: /home/pi/xpertiz/comp3.mp3")
    pg.mixer.music.play()	
    while pg.mixer.music.get_busy():
            # check if playback has finished
            clock.tick(30)
    print ("End play music")
    

while True:
    currentDT = datetime.datetime.now()
    if (currentDT.hour >= 23 or currentDT.hour <= 6):
        print ("Sleep Time between 23h00 and 06h05.")
        time.sleep(300)
    else:   
        i=GPIO.input(7)
        if i:
            print ("Someone in the ROOM.")
            launchMp3()
            launchMp3()      
        time.sleep(1)
