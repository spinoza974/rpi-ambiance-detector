# *********************************************************************************************
# Description: Script Python to launch music if sensors (light and motion) are in good status
# Author: J.Vergereau
# Date: 18/01/2022
# *********************************************************************************************

import pygame as pg
import time
import RPi.GPIO as GPIO

# Read on GPIO 11 Light Sensor result
#GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(4,GPIO.IN)



def launchMp3():
    
    # 1. Load mp3 sound  
    freq = 44100 # audio CD quality
    bitsize = -16 # unsigned 16 bit
    channels = 2 # 1 is mono, 2 is stereo
    buffer = 2048 # number of samples (experiment to get best sound)
    clock = pg.time.Clock()
    pg.mixer.init(freq, bitsize, channels, buffer)
    pg.mixer.music.set_volume(0.8)
    pg.mixer.music.load('/home/pi/xpertiz/comp3.mp3')
    
     # 2. play a short mp3 sound to notify Start of taking a Photo
    print ("Start play music: /home/pi/xpertiz/comp3.mp3")
    pg.mixer.music.play()	
    while pg.mixer.music.get_busy():
            # check if playback has finished
            clock.tick(30)
    print ("End play music")
    

isPlayingMusic = "false"
while True:
    i=GPIO.input(4)

    print ("Light detected: " + str(i))
    time.sleep(1)
    if i == 0:
        isPlayingMusic = "true"
        launchMp3()
        print ("Light ON: start play music: " + isPlayingMusic)
    else:
        isPlayingMusic = "false"
        print ("Light Off: stop music: " + isPlayingMusic)
        time.sleep(1)

